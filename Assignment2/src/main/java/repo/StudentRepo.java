package repo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Student;

public class StudentRepo {

	public void insertStudent(Student s){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        EntityManager eniEntityManager = entityManagerFactory.createEntityManager();

        eniEntityManager.getTransaction().begin();
        eniEntityManager.merge(s);

        eniEntityManager.getTransaction().commit();
        entityManagerFactory.close();
    }
}
