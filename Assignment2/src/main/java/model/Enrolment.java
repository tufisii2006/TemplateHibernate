package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="enrolment")
public class Enrolment {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	//student
	//course
	@Column(name="grade")
	private int grade;
	public Enrolment(){
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
}
