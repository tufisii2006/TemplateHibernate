package appStart;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Student;
import repo.StudentRepo;

public class AppStart {
	

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("org.hibernate.tutorial.jpa");
		Student s = new Student();
		StudentRepo repo = new StudentRepo();
		
		s.setName("Ana-Rednic");
		s.setCnp("290606");
		s.setEmail("ana_rednic@yahoo.com");
		s.setGroup("30432");
		s.setIdentificationNr("20453");

		repo.insertStudent(s);
		
		/***/
		
	}
}
